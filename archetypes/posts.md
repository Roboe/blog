---
title: "{{ replace .Name "-" " " | title }}"
draftDate: {{ .Date | time.Format "2006-01-02" }}
date: {{ .Date | time.Format "2006-01-02" }}
categories:
{{- range $key, $value := .Site.Taxonomies.categories }}
  - {{ $key }}
{{- end }}
image: dirección-relativa.jpg
description: >
  {{ replace .Name "-" " " | title }}
reposts:
  mastodon: https://mastodon.social/@RoboePi/<tootId>
---

contenido

~{{< author >}}



[enlace]: https://blog.virgulilla.com
