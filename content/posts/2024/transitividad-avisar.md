---
title: "La transitividad alternativa de un aviso"
draftDate: 2024-06-30
date: 2024-07-02
categories:
- reseñas
description: >
  Una pequeña historia personal de cómo caí en una hipercorrección que afectó a la transitividad del verbo «avisar» y cómo disfruté desentrañando el caso.
---

Esta entrada es un tanto distinta porque trata de lingüística. En concreto, de un caso particular de gramática con el que me he topado, y cuyo análisis me ha resultado emocionante. La lengua es una de mis pasiones: estuve a punto de estudiar Filología Hispánica (antes de que hubiera carreras de lingüística), en un momento particular me formé como corrector de estilo por puro placer y poco después participé de un grupo de amigos amantes y profesionales de la lingüística (¡hola, lenguaraces! 👋😛).

El caso tiene como contexto una obra de Juan Mayorga, _Hamelin_ (2005), de la que estábamos haciendo una lectura dramatizada. En un momento de la obra, figura el siguiente texto:

<figure>

> Montero llama a su secretaria. Le pide que telefonee a Julia para avisarle que él no podrá recoger a Jaime, que por favor lo haga ella.

</figure>

Por mi parte, yo he leído: «[...] que telefonee a Julia para avisar**la** de que él no podrá recoger a [...]». Mi amiga M. ha observado que el pronombre enclítico «-la» era [laísmo][fundeu-lxismo], a lo que yo he respondido, confundido, que no me sonaba como tal. Aunque nunca he tenido un problema de laísmo, sinceramente lo pensaba por costumbre, no racionalmente. Y como los correctores tienen que ser fundamentalmente humildes porque también se equivocan, me interesaba poder justificar mi intuición o aprender de mi error.

Tras comprobarlo, a M. no le faltaba razón... y curiosamente, ambos la teníamos. Si quieres seguir mi proceso de pensamiento, entonces sigue leyendo; aunque si no te interesa, puedes [saltar al resumen final](#en-resumen) directamente.

## El complemento de régimen para no comerse una preposición

Si eres un lector atento, habrás observado que en mi formulación no solo cambio el pronombre, sino que añado un «de». Al leer el texto original, entendí que se había producido un queísmo, así que lo corregí de manera inconsciente.[^corregir-academico] Esto tiene que ver con que se puede expresar la [transitividad][wes-transitividad] de algunos verbos mediante un [complemento de régimen preposicional][wikilengua-creg] y los hablantes de español de los que me rodeo, habitualmente, «avisan **de** algo a alguien».

Partiendo desde ahí, para comprobar si efectivamente el pronombre «-la» es un caso de laísmo, hay que analizar si el sintagma nominal «a Julia» al que equivale es un complemento directo; si por el contrario es un complemento indirecto, entonces el pronombre adecuado sería «-le».[^leismo-admitido] Algunos hablantes tienen dificultades para identificar los complementos directos cuando son personas, lo que puede generar leísmo o dificultar el análisis sintáctico.

Una manera de encontrar el complemento directo de una oración es pasarla a voz pasiva: «[...] para que Julia sea avisada de _aquello_». El sujeto paciente, en este caso Julia, es aquel sobre quien recae la acción del verbo en la voz pasiva; en la voz activa, esa definición es la del complemento directo. Es decir, Julia era el complemento directo de la frase, así que su pronombre enclítico equivalente debe ser «-la».

Entonces caí en que había añadido el «de».

## La ambigua transitividad de «avisar»

Por pura costumbre volví a consultar la [definición de queísmo de la Fundéu][fundeu-queismo]. Y entonces leí: «Hay, por último, verbos que pueden construirse con, o sin, la preposición _de_. Es el caso de [...] _avisar_ algo a alguien y _avisar de_ algo a alguien; [...]». Un momento, ¿cómo? Necesitaba saber más sobre cuándo se usa de una manera o de otra.

Uno de los recursos más valiosos para resolver dudas en español, sin caer en los usos de tu propia variedad geográfica, es el Diccionario Panhispánico de Dudas. Y allí me fui a consultar la [entrada de _avisar_][dpd-avisar], que recoge, amplía y da ejemplos sobre las dos formas válidas con las que se expresa la transitividad del verbo «avisar».

Eso cambia las tornas completamente, porque sin añadir el «de», si buscamos el complemento directo usando la pasiva («[...] para que _aquello_ sea avisado a Julia»), ¡Julia no es el complemento directo, sino el indirecto! Luego, efectivamente, ¡el pronombre enclítico que le corresponde es «-le»! Como bien decía M., usar «-la» ahí es un caso de laísmo.

## En resumen

El verbo «avisar» con el sentido de «advertir algo a alguien» es transitivo, y según afirma el [Diccionario Panhispánico de Dudas][dpd-avisar], se puede construír de dos maneras distintas: _avisar_ algo y _avisar de_ algo. La oración, además, es una subordinada de varios niveles, lo que añade complejidad al análisis.

La formulación original pertenece al caso que expresa la transitividad de _avisar_ mediante un complemento directo (DPD avisar 1.b), no de régimen. Como recoge el DPD, en esa forma requiere de un complemento indirecto, que en este caso es Julia. Así que si alguien dice «avisarla que [...]» en lugar de «avisarle que [...]» está cometiendo laísmo o queísmo, pero solo tiene que corregir uno de los dos.

Por el contrario, la formulación que a mí me resulta más natural, «avisarla de que [...]» pertenece al caso que expresa la transitividad de _avisar_ con un complemento preposicional de régimen, «de que [...]» o «de algo» (DPD avisar 1.a). En este caso, Julia es el complemento directo, que al referenciarse como pronombre enclítico en el verbo, debe concordar con el género gramatical de Julia. Así que si alguien dice «avisarle de que [...]» está cometiendo leísmo o dequeísmo, pero solo tiene que corregir uno de los dos.

Nunca me había encontrado con una frase en la que se pudieran analizar dos usos gramaticales no normativos distintos pero excluyentes. ¡Siempre se aprende algo nuevo cuando se mira con los ojos de la curiosidad!

~{{< author >}}


[wikilengua-creg]: https://www.wikilengua.org/index.php/Complemento_de_r%C3%A9gimen
[wes-creg]: https://es.wikipedia.org/wiki/Complemento_de_r%C3%A9gimen
[wes-transitividad]: https://es.wikipedia.org/wiki/Transitividad_(gram%C3%A1tica)
[wes-titivillus]: https://es.wikipedia.org/wiki/Titivillus

[dpd-avisar]: https://www.rae.es/dpd/avisar
[dpd-leismo-excepcion]: https://www.rae.es/dpd/le%2525C3%2525ADsmo#S1590507320049776722

[fundeu-queismo]: https://www.fundeu.es/recomendacion/queismo/
[fundeu-lxismo]: https://www.fundeu.es/recomendacion/leismo-laismo-y-loismo-claves/

[^corregir-academico]: Incidentalmente, Juan Mayorga es un dramaturgo que ocupa uno de los asientos de la Real Academia de la Lengua. Puede sonar arrogante corregir a un académico, pero como soy consciente de lo complejo de los procesos editoriales, en realidad achaqué el descuido a la transcripción. ¡[Titivillus][wes-titivillus] tiene la mala costumbre de cebarse con ellos! 😈

[^leismo-admitido]: Si Julia hubiera sido un hombre (o adoptase los pronombres masculinos), entonces usar el pronombre «-le» hubiera sido correcto tanto si forma parte del complemento directo como del indirecto. Hay un leísmo admitido por la Academia que sucede cuando el referente es de género masculino (más información en el [DPD][dpd-leismo-excepcion]). Si eres de los que dicen «le quiero» en lugar de «lo quiero» cuando te refieres a una persona, eres de una minoría lingüística y probablemente vivas en el centro peninsular de España.