---
title: "Explicándole nuestra medición del tiempo a un extraterrestre"
draftDate: 2024-11-12
date: 2024-12-01
categories:
  - ficción
  - traducción
description: >
  Un diálogo ficticio entre un extraterrestre pacífico y un terrestre que le explica cómo medimos el tiempo en la Tierra. Traducción y adaptación propia de una historia original de Foone.
reposts:
  mastodon: https://mastodon.social/@RoboePi/<tootId>
draft: true
---

Uno de los temas que más me apasionan es el tiempo y todo lo que tiene que ver con él, [como ya mostré con mi charla en la quinta edición del K4mpKrusty]({{< relref k4mpkrusty-v >}}). Desde su misma concepción, pasando por nuestra percepción y la mitología hasta llegar a la horología, pensar sobre el tiempo es sumergirse en casi todas las materias del conocimiento y su historia. No solo en las ciencias naturales, sino también —y sobre todo— en las ciencias sociales.

El 20 de septiembre de 2022, la [programadora Foone](https://wiki.foone.org) escribió [una historia en Twitter](https://twitter.com/Foone/status/1572260363764400129) que disfruté y con la que me reí y aprendí a partes iguales. Al final de su hilo, indicaba informalmente que cualquiera podía adaptar esa historia siempre que indicase su autoría. Y como la original estaba en inglés, la he traducido y adaptado para que el público hispanohablante lo pueda disfrutar también.

Como el original tiene mucha dependencia del lenguaje y referencias culturales que he tenido que omitir o adaptar, también incluyo debajo de mi traducción [el texto original en inglés](#texto-original-en-inglés).

~{{< author >}}

---

<blockquote>

Algún día los extraterrestres aterrizarán sus platillos en el [aeropuerto de Manises][wes-incidente-de-manises] y todo irá bien hasta que tratemos de explicarles nuestro calendario.

—Mira, dividimos el año en varias subunidades llamadas «meses», compuestos de varios días, y no todos tienen la misma extensión.

—Entiendo que es inevitable si vuestro número de rotaciones por órbita es un número primo. 

—Claro, salvo que nuestro número no es primo.

—Pero seguramente tenéis la mayoría de esos «meses» de la misma extensión y solo acortáis o alargáis el último, ¿no?

—No... Tienen distintas extensiones sin seguir un patrón lógico.

—¿Perdón?

—Y además subdividimos los meses en «semanas», que son de siete días.

—¡Aaah! ¿Así que cada mes es un número entero de semanas? 

—Eso tendría sentido, pero no. Solo uno, a veces.

—¡A VECES!

—Sí. Nuestra órbita alrededor del Sol no dura un número entero de días, así que tenemos que cambiar el número de días en el año de tanto en tanto.

—Oh, sí; algo similar ocurre en Epsilon Indi 7, donde tienen que añadir un día extra cada 39 años para no desincronizar las vacaciones.

—¡Sí, así es como funciona el nuestro! Aunque la ratio no funciona tan limpiamente, así que lo añadimos cada cuatro años, salvo que sea múltiplo de cien, pero siempre si también es múltiplo de cuatrocientos.

—Oh, ¿entonces contáis los años? ¿Cuál es la época de referencia?

—Uh, se supone que es el nacimiento de un líder religioso, pero calcularon mal y está desfasado cuatro años, si es que existió.

—¿«Si»? ¿Basáis vuestro calendario en el nacimiento de alguien de quien no estáis seguros que existiera?

—Sí. Se ha escrito sobre él en un famoso libro, pero los registros históricos son insuficientes.

—Interesante. No me había dado cuenta de que vuestro planeta es uno de los que tienen una única religión universal; eso normalmente solo pasa en mentes colmena parciales o totales.

—Oh, no la tenemos.

—¡No la tenéis!

—Sí, tenemos diferentes religiones.

—Ah, pero todas ellas provienen de una antecesora común que conviene en la existencia de ese líder, ¿verdad?

—Uy, no. Dos de las mayoritarias sí coinciden, pero la mayoría de las demás no creen en él.

—¿BASÁIS VUESTRO CALENDARIO EN UN LÍDER RELIGIOSO EN EL QUE NO TODOS CREÉIS?

—Sí, bueno, en su nacimiento. Aunque, claro, nos equivocamos por unos años.

—Vale, de acuerdo. Así que tenéis unas intrincadas reglas sobre cuándo cambiáis la longitud de vuestros años. Y me asusta preguntar esto, pero... sin duda añadís ese día extra al final, ¿verdad?

—... nop.

—¿Al principio del año, pues?

—Qué va. Al final del segundo mes.

—¡POR QUÉ EN EL SEGUNDO MES!

—No estoy seguro, en realidad.

—Vaya. Pues en este momento temo preguntar, pero ¿cómo medís el tiempo que comprende cada día?

—Oh, eso es mucho más sencillo. Cada día se divide en horas, cada hora tiene minutos y cada minuto tiene segundos.

—Bien. ¿Y diez de cada?

—¿Diez horas? No. Hay 24 horas, 60 minutos, 60 segundos.

—... te entendí que usábais un sistema decimal de numeración.

—¡Y lo usamos! Casi siempre. Pero nuestro sistema horario se originó en una civilización desaparecida hace mucho que prefirió el sistema sexagesimal hace unos 5000 años.

—¿Y no lo habéis cambiado desde entonces?

—No.

—Hum. Vale, ¿entonces por qué 24? No es divisor de 60.

—Ah, ¡porque en realidad es 12!

—¿Perdón?

—Sí, mira: cada día es de 24 horas, pero está dividido en dos partes de doce.

—Y sesenta son cinco doces, vale. Tiene cierta lógica. Así que, después de la hora 12, pasa a ser la segunda mitad, que es 1, ¿no?

—No, después de las 11.

—¡Ah, que empezáis a contar en cero! Así que son las horas 0 a 11 en la primera mitad ¿y luego 12 a 23 en la segunda mitad?

—No. De 12 a 11 en la primera mitad y también en la segunda mitad.

—Por favor, explícame eso antes de que se me derrita el cerebro por la boca. 

—Primero son las 12. La siguiente hora es la 1, que continúa hasta las 11, y luego vuelve a las 12.

—Así no es como funcionan los números. ¿Y cómo diferenciáis las primeras 12 de las segundas 12?

—¡Ah, no usamos números para eso!

—¿No numeráis las dos mitades de vuestro día?

—Qué va, las denominamos a. m. y p. m.

—¡Y ESO QUÉ SIGNIFICA!

—Creo que ¿_ante meridiem_ y _post meridiem_? No estoy seguro, no sé mucho latín.

—¿Latín?

—Sí, es una lengua ancestral originaria de un antiguo imperio que controló una gran parte del mundo y de la que aún usamos algunos de sus términos.

—Ah, y esa fue la civilización que prefería el sistema sexagesimal y estableció vuestro sistema temporal, ¿no?

—Eso tendría sentido, pero... no, es una que no tiene nada que ver.

—Entendido. ¿Y qué hacéis cuando queréis medir tiempos muy cortos, más cortos que un segundo?

—Ah, pues usamos milisegundos y microsegundos.

—¡Oh! Esos son una sesentava parte de un segundo, y después otra sesentava parte de los anteriores, ¿no?

—No. Milésimas.

—¿Así que por fin cambiáis al sistema decimal? Pero solo para las subdivisiones de un segundo.

—Eso es.

—Pero por miles, o sea, diez decenas de decenas.

—Sí. Técnicamente tenemos decisegundos y centisegundos, que son una décima parte de un segundo y una centésima parte de un segundo respectivamente, pero nadie las usa en realidad. Sencillamente usamos mili.

—Eso se parece más a un sistema de numeración milesimal que decimal.

—Supongo que sí. Hacemos algo parecido con medidas de volumen, de distancia o de masa.

—¿Y aún así lo llamáis decimal?

—Ajá.

—Vale, a ver si lo he entendido bien: vuestro año se divide en diez meses, cada uno de los cuales tiene siempre un número distinto de días, salvo el segundo, que varía según una compleja fórmula... y cada día se divide en dos mitades de 12 horas, luego 60 minutos, 60 segundos, 1000 milisegundos, ¿verdad?

—Doce meses, en realidad.

—Claro, por esa civilización ancestral que prefería el sexagesimal, y doce es un divisor de sesenta.

—En realidad no, eso viene de la civilización del latín. Antes hubo diez.

—¿Perdón?

—Sí, la civilización de origen latino añadió dos meses en mitad de su mandato, y desfasó el calendario otros dos meses. Por eso algunos se nombran con un número incorrecto.

—Acabas de decir dos cosas que me cuesta entender: uno, ¿vuestros meses se nombran, no se numeran?; y dos, ¿LOS NOMBRES ESTÁN MAL  ?

—¡Exacto! Nuestro noveno mes lleva el nombre del número siete, y así con el décimo, undécimo y duodécimo.

—¿Vuestro duodécimo mes se llama... décimo?

—Ajá.

—¡De dónde vendrán los demás nombres!

—De diferentes cosas. Principalmente dioses o gobernantes.

—Ah, de esa religión de la que viene vuestra época de referencia.

—Oh... no. De otra distinta.

—¿Así que tenéis una época de referencia que viene de una religión, pero nombráis vuestros meses con respecto a otra distinta?

—¡Eso es! Y espera a escuchar sobre los días de la semana.

—¡QUÉ!

—Mira, agrupamos los días en periodos de siete días...

—Que no es un divisor de la longitud de vuestros meses ni vuestros años, ¿no?

—Correcto. No interrumpas.

—Discúlpame.

—Pero nombramos los días de la semana, en lugar de numerarlos. Eso es curioso, también: hay discrepancia en cuál de los días empieza la semana.

—¿Tenéis un periodo con una cadencia de siete días y no coincidís en cuándo empieza?

—Sí, o el lunes, o el domingo.

—Y esos nombres vienen de...

—__¡Cuerpos celestes y dioses! El Sol y la Luna dan lugar al domingo y al lunes, por ejemplo.__

—Pero... Miré los parámetros de órbita de vuestro planeta. ¿El Sol no sale todos los días?

—Así es.

—Ah, ¿entonces tenéis una de esas órbitas extrañas en la que el satélite natural está más cerca o se produce un eclipse cada siete días, como en Quagnar 4?

—No. El Sol y la Luna son iguales cada día. Teníamos que nombrar los días de alguna manera.

—Y el resto de días, ¿esos los nombráis según dioses?

—__¡Eso es!__

—¿Según vuestra religión mayoritaria, imagino?

—__Qué va. Esa (junto con la segunda más mayoritaria, por cierto) reconocen un solo dios, y no tiene un nombre concreto.__

—Oh. ¿Entonces de qué religión vienen? ¿De la de origen latino otra vez?

—__No, ellos solo nombraron uno de los días con nombre de dios.__

—Solo uno... ¡ENTONCES LOS DEMÁS DÍAS SON DE UNA RELIGIÓN COMPLETAMENTE DISTINTA!

—¡Sí!

—La tercera o cuarta mayoritaria, espero.

—__No, qué va. Es una que... ¿ya no existe? Prácticamente murió hace 800 años, aunque tiene algunos pequeños resurgimientos modernos, desde luego.__

—De acuerdo, déjame comprobar que lo he entendido hasta ahora. Contáis vuestros días, horas, segundos y fracciones menores mediante un patrón repetitivo. Pero vuestros años se basan en una época de referencia religiosa, pese a tratarse solo de una religión de entre varias.

—Vas bien.

—Por el contrario, nombráis vuestros meses y días, aunque algunos llevan el nombre de números, pese a ser los que no corresponden.

—Efectivamente.

—__Y los que no son números o regentes u objetos celestes son dioses, ¿correcto?__

—¡Correcto!

—Pero los meses y los días de la semana llevan nombres de dioses de distintas religiones con respecto a la de la época de referencia, y de hecho, entre sí?

—__¡Justo! Excepto el sábado. Eso es de la misma religión que la de los meses.__

—__Y la religión de los meses y el sábado es de la misma cultura que os dio el sistema de doce meses, además de los nombres de las dos mitades del día, que también nombráis, ¿no?__

—¡Eso es! Bueno, casi.

—Por favor, explícate lenta y comprensiblemente.

—De acuerdo. Las culturas hasta entonces tenían años de doce meses a causa de la Luna. Sin embargo, venían de usar años de diez meses antes de cambiar a doce y darle los nombres modernos.
"yeah so cultures before then had a 12 month system, because of the moon. But they had been using a 10 month system, before switching to 12 and giving them the modern names"

—¿La... Luna? ¿Vuestro cuerpo celeste?

—Sí. Completa una órbita en alrededor de 27 días, o sea, unas doce veces al año, por lo que resulta natural dividir el año en doce periodos que eventualmente se llamaron meses.

—Vale, eso tiene sentido. Espera, no. Vuestro periodo orbital es aproximadamente 365,25 días, ¿verdad?

—Ajá. Por eso hacemos 365 o 366 según la fórmula.

—Pero eso no encaja. 365 dividido por 27 son ~13,5, no 12.

—Es verdad... entonces no estoy seguro de por qué doce era tan común. ¿Quizá venía de la civilización del sistema sexagesimal?[^lunaciones]

—Está bien. Una última comprobación antes de que envíe el informe: vuestros años se enumeran según un líder religioso; los años siempre tienen doce meses, pero la longitud de esos meses no es consistente entre ellos ni con respecto a otros años.

—¡No olvides que la época de referencia es inexacta!

—Claro, cierto. Y vuestros meses tienen nombre, algunos de otra religión diferente y otros de números, pero no del número que ocupa el mes en el año.

—Eso es. Y cuando cambiamos la longitud de los meses, es el segundo mes el que cambiamos.

—__¡Cómo podría olvidarlo! Además de los meses, tenéis «semanas» de siete en siete días, que llevan el nombre de dioses de dos religiones, una de las cuales es la misma que los nombres de los meses y la otra ya está extinta. Y no convenís en qué día empieza la semana.__

—¡Nop! Yo apostaría por el lunes.

—¿Ese mismo lunes que nombráis en honor a vuestra luna, que supuestamente influenció el criterio común de ciclos de doce meses por año, pese a que orbita trece veces cada año?

—¡Correcto!

—__Y con respecto a vuestros días, se dividen en dos mitades, nombradas según una locución que no entendéis realmente en la lengua muerta de la misma civilización que dio nombre a los meses y al sábado.__

—Exacto. Aprendí algo en el instituto, pero solo recuerdo cosas como «chico», «chica», «apestoso», «chupapollas».

—Encantador. Y después, cada mitad se divide en doce horas, pero empezáis en la hora 12, luego saltáis a la 1 y continuáis hasta las 11.

—Todo lo que puedo decir es que tiene más sentido en los relojes analógicos.

—No sé lo que es eso y llegados a este punto prefiero que no profundices. Así que cada hora de esas se divide en sesenta minutos y luego estos en sesenta segundos, lo que proviene de una antigua civilización, pero no de la que dio nombre a vuestros meses.

—Eso es. Distinta peña. Distinta parte del globo.

—Bien. Y más allá de los segundos, pasáis al sistema decimal de números, aunque en la práctica solo utilizáis múltiplos de mil. ¿Eran milisegundos y microsegundos?

—Correcto. Y hay aún más pequeños, pero todos de mil en mil.

—Perfecto. Lo tengo. Todo escrito aquí. Ahora, si me disculpas, tengo que ir a asegurar que no me he dejado encendido el interocitor. Vuelvo ya mismo.

El espigado extraterreste regresa a su platillo sin hacer ningún gesto. La rampa de desembarque se cierra. La nave despega suavemente mientras se repliegan sus larguiruchas patas. Se oye un chasquido, seguido de un silbido súbito cuando el aire reclama el espacio que ocupaba la nave, ahora repentinamente vacío. Las alarmas antiaéreas de la Agencia Europea de Defensa saltan brevemente cuando detectan que un objeto abandona la atmósfera terrestre a una fracción importante de la velocidad de la luz.

Durante los años posteriores se consiguieron muchos avances tecnológicos gracias a lo que olvidaron aquí, un pequeño objeto con forma de tableta hecho de algún tipo de piedra artificial o material compuesto de neutrino. Eventualmente se tradujo el mensaje que quedó en pantalla como «Documento sin título 1 no se ha guardado, ¿estás seguro de que quieres salir? (sí) (no) (cancelar)».

Han pasado muchos años y esperamos el día en que regresen los extraterrestres.

Siguen sin hacerlo.

Con nuevos desarrollos propios, construímos sistemas de radar espaciales y podemos observar a las distintas especies navegando por la galaxia. No mucho después comprendimos que estaban dando un amplio perímetro a la Tierra intencionadamente. Naves no tripuladas cruzan la galaxia, pero cuando llegan a un año luz de distancia de la Tierra, se desvían para evitarla. Finalmente conseguimos hacer funcionar una radio subespacial y empezamos a descifrar el abundante tráfico de millares de civilizaciones comunicándose entre sí. Enviamos un mensaje de saludo y paz.

Menos de una semana después, la red subespacial se silencia. Nuestro radar espacial indica que el sistema solar está ahora rodeado de pequeñas naves, sospechosas de tratarse de sondas automatizadas de alguna clase, que bloquean activamente todo el tráfico de radio entrante o saliente. Incluso los púlsares quedan en silencio; todas las ondas de radio han desaparecido. Nos concentramos en desentrañar el secreto de viajar aún más rápido que la luz. El primer prototipo nunca consigue alzarse del suelo, porque antes de que el cohete pueda siquiera entrar en ignición, lo aplasta un pequeño meteoro.

La investigación forense apunta a que se trata de un reloj de sol, tallado en roca extraída de la cara oculta de la Luna.

</blockquote>


[wes-incidente-de-manises]: https://es.wikipedia.org/wiki/Incidente_ovni_de_Manises
[wes-religiones-por-adeptos]: https://es.wikipedia.org/wiki/Principales_grupos_religiosos#Religiones_más_grandes_según_su_número_de_adeptos
[wes-sabbat]: https://es.wikipedia.org/wiki/Sabbat

---

## Texto original en inglés

(Enriquecida con enlaces para los curiosos. [Copia en texto plano del original](./source-foone-alien-time.txt).)

<blockquote lang="en">

Someday aliens are going to land their saucers in a field somewhere [in New Jersey][wen-morristown-ufo-hoax] and everything is going to go just fine right up until we try to explain our calendar to them:

"yeah we divide our year into a number of sub units called 'months' made up a number of days, and they're not all the same length"

"I guess that's unavoidable, if your [rotations-count per orbit][wen-earths-orbit] is a prime number"

"yeah, our's isn't prime"

"but surely you have most of these 'months' the same length and just make the last one shorter or longer?"

"No... They're different lengths following no logical pattern"

"what"

"and we further subdivide the months into 'weeks', which is 7 days."

"ahh, so each month is an integer multiple of weeks?"

"that would make sense, but no. [Only one is, sometimes][wen-february-patterns]"

"SOMETIMES?!"

"yeah our orbit around the sun isn't an integer number of days, so we have to change the number of days to in a year from time to time"

"oh yes, a similar thing happens on Epsilon Indi 7, where they have to add an extra day every 39 years to keep holidays on track"

"yeah that's how ours work! Although the ratio doesn't work out cleanly, so we just do every [4 years, except every 100 years, except except every 400 years][wen-leap-year]"

"oh, you number your years? What's the epoch?"

"uh, it's supposed to be the [birth of a religious leader][wen-nativity-of-jesus], but they got the math wrong so it's [off by 4 years][wen-birthdate-of-jesus], if he existed at all."

"if? You based your calendar off the birth date of someone you're not sure exists?"

"yeah. He's written about in [a famous book][wen-new-testament] but historical records are spotty."

"interesting. I didn't realize your planet was one of the ones with a single universal religion, that usually only happens in partial or complete hive minds."

"uhh, we're not."

"You're not?!"

"yeah we have multiple religions."

"oh but they all have a common ancestor, which agrees on the existence of that leader, right?"

"uh, no. [Two of the big ones do][wen-perspectives-on-jesus], but most of the others don't believe in him"

"YOUR CALENDAR IS BASED ON A RELIGIOUS LEADER THAT NOT EVERYONE BELIEVES IN?"

"well, on his birth. And yeah, we got it wrong by a couple years."

"OK, fine. So, you have somewhat complicated rules about when you change the length of your years, and I'm scared to ask this, but... You definitely just add or subtract that extra day at the end, right?"

".... Nope."

"At the start of the year? "

"nah. The end of the second month"

"WHY WOULD IT BE THE SECOND MONTH?"

"I'm not sure, really."

"huh. So at this point I'm dreading asking this, but how do you measure time within each day?"

"oh that's much simpler. Each day is divided into hours, each hour has minutes, and each minute has seconds."

"ok. And 10 of each?"

"10 hours? No. There's 24 hours, 60 minutes, 60 seconds"

".... I thought you said you used a base-10 counting system"

"we do! Mostly. But our time system came from some [long gone civilization][wen-babylonian-numerals] that liked base-60 like 5000 years ago"

"and you haven't changed it since?"

"No."

"huh. Okay, so why 24? That's not a divisor of 60"

"oh because it's actually 12!"

"what"

"yeah each day is 24 hours but they are divided into two sets of 12."

"and that's 5 12s, right, I see the logic here, almost. So like, after hour 12, it becomes the second half, which is 1?"

"No, after 11."

"oh, you zero-index them! So it's hours 0-11 in the first half, then 12-23 in the second half?"

"No. 12 to 11 in the first half, and again in the second half"

"please explain that before my brain melts out my mouth"

"the first hour is 12. Then the next one is 1, then it goes back up to 11, then 12 again"

"that is not how numbers work. And how do you tell first 12 apart from second 12?"

"oh we don't use numbers for that!"

"you don't number the two halves of your day?"

"nah, we call them AM and PM"

"WHAT DOES THAT MEAN"

"I think it's [ante-meridian and post-meridian][wen-am-pm]? But I'm not sure, I dont know much Latin"

"Latin?"

"yeah it's an ancient language from an [old empire which controlled a lot of the world][wen-roman-empire] and we still use some of their terms"

"oh, and that was the civilization that liked base-60 and set up your time system?"

"that would make sense, but... No, completely different one."

"okay, and what do you do to if you want to measure very short times, shorter than a second?"

"oh we use milliseconds and microseconds"

"ahh, those are a 60th of a second and then 60th of the other?"

"No. Thousandths."

"so you switch to base-10 at last, but only for subdivisions of the second?"

"yeah."

"but at thousands, ie, ten tens tens"

"yeah. Technically [we have deciseconds and centiseconds][wen-si-prefixes], which are 1/10 of a second, and 1/100 of a second, but no one really uses them. We just use milli."

"that seems more like a base-1000 system than a base-10 system."

"it kinda is? We do a similar thing with measures of volume and distance and mass."

"but you still call it base-10?"

"yeah"

"so let me see if I get this right:
Your years are divided in 10 months, each of which is some variable number of days, the SECOND of which varies based on a complex formula...
and each day is divided into two halves of 12 hours, of 60 minutes, 60 seconds, 1000 milliseconds?"

"12 months, actually."

"right, because of the ancient civilization that liked base-60, and 12 is a divisor of 60."

"No, actually, that came from the civilization that used latin. Previously [there were 10][wen-roman-10-months-calendar]."

"what"

"yeah the Latin guys added two months part of the way through their rule, [adding two more months][wen-roman-republican-calendar]. That's why some are named after the wrong numbers"

"you just said two things I am having trouble understanding.\
1\. Your months are named, not numbered?\
2\. THE NAMES ARE WRONG?"

"yep! Our 9th month is named after the number 7, and [so on for 10, 11, and 12][wen-ordinal-months]."

"your 12th month is named... 10?"

"yeah."

"what are the other ones named after?!"

"various things. Mainly Gods or rulers"

"oh, from that same religion that your epoch is from?"

"uh... No. [Different one][wen-months-etymology]."

"so you have an epoch based on one religion, but name your months based on a different one?"

"yeah! Just wait until you hear about days of the week."

"WHAT"

"so yeah we group days into 7-day periods-"

"which aren't an even divisor of your months lengths or year lengths?"

"right. Don't interrupt"

"sorry"

"but we name the days of the week, rather than numbering them. Funny story with that, actually: there's hdisagreement about which day starts the week."

"you have a period that repeats every 7 days and you don't agree when it starts?"

"yeah, it's Monday or Sunday."

"and those names come from..."

"celestial bodies and gods! The sun and moon are Sunday and Monday, for example"

"but... I looked at your planet's orbit parameters. Doesn't the sun come up every day?"

"yeah."

"oh, do you have one of those odd orbits where your natural satellite is closer or eclipsed every 7 days, like Quagnar 4?"

"no, the sun and moon are the same then as every other day, we just had to name them something."

"and the other days, those are named after gods?"

"yep!"

"from your largest religion, I imagine?"

"nah. [That one][wen-god-name-in-christianity] (and [the second largest][wen-god-name-in-islam], actually) only has one god, and he doesn't really have a name."

"huh. So what religion are they from? The Latin one again?"

"nah, they only named [one of the God-days][wen-saturn-day]"

"only on... SO THE OTHER DAYS ARE FROM A DIFFERENT religion ENTIRELY?"

"Yep!"

"the third or forth biggest, I assume?"

"nah, it's [one that][wen-germanic-weekdays]... Kinda doesn't exist anymore? It mostly died out like 800 years ago, though there are some modern small revivals, of course"

"so, let me get confirm I am understanding this correctly.\
Your days and hours and seconds and smaller are numbered, in a repeating pattern. But your years are numbered based on a religious epoch, despite it being only one religion amongst several."

"correct so far"

"and your months and days of the week are instead named, although some are named after numbers, and it's the wrong numbers"

"exactly"

"and the ones that aren't numbers or rulers or celestial objects are named after gods, right?"

"yup!"

"but the months and the days of the week are named after gods from different religions from the epoch religion, and indeed, each other?"

"yeah! Except Saturday. That's the same religion as the month religion"

"and the month/Saturday religion is also from the same culture who gave you the 12 months system, and the names for the two halves of the day, which are also named?"

"right! Well, kinda."

"please explain, slowly and carefully"

"yeah so cultures before then had a 12 month system, because of the moon. But they had been using a 10 month system, before switching to 12 and giving them the modern names"

"the... Moon? Your celestial body?"

"yeah, it completes an orbit about every 27 days, so which is about 12 times a year, so it is only natural to divide the year into 12 periods, which eventually got called months"

"ok, that makes sense. [Wait, no][wen-lunar-calendar-issues]. Your orbital period is approximately 365.25 days, right?"

"yeah. That's why we do 365 or 366 based on the formula"

"but that doesn't work. 365 divided by 27 is ~13.5, not 12"

"yeah I'm not sure why 12 was so common then. Maybe it goes back to [the base 60 people][wen-babylonian-months]?"[^lunaciones]

"okay so one final check before I file this report:\
Years are numbered based on a religious leader. Years always have 12 months, but the lengths of those months is not consistent between each other or between years."

"don't forget the epoch we number our years from is wrong!"

"right, yes. And your months are named, some after a different religion, and some after numbers, but not the number the month is in the year."

"right. And when we change the month lengths, it's the second one we change"

"how could I forget?\
After months you have a repeating 'week' of 7 days, which is named after gods from two religions, one of which is the month-naming one, and a nearly extinct one. And you don't agree when the week starts."

"nope! My money is on Monday."

"that's the Monday that's named after your moon, which supposedly influenced the commonality of the 12 months in a year cycle, despite it orbiting 13 times in a year?"

"correct!"

"and as for your days, they split into two halves, named after a phrase you don't really understand in the long dead language of the same culture that named the months and Saturday."

"Yep. I took some in college but all I remember is like, 'boy', 'girl', ['stinky', 'cocksucker'][wen-latin-obscenity]"

"charming. And then each half is divided into 12 hours, but you start at 12, then go to 1, and up to 11"

"all I can say is that it makes more sense on analog clocks."

"i don't know what that is and at this point I would prefer you not elaborate.\
So each of those hours is divided into 60 minutes and then 60 seconds, and this comes from an ancient civilization, but not the one that gave you the month names"

"yep. Different guys. Different part of the world."

"ok. And then after seconds, you switch to a 'base-10' system, but you only really use multiples of a thousand? Milliseconds and microseconds?"

"right. And there's smaller ones beyond that, but they all use thousands"

"right. Got it. All written down here. Now if you'll excuse me, I just gotta go make sure I didn't leave my [interociter][wen-interocitor] on, I'll be right back."

The tall alien walks back into their saucer without a wave. The landing ramp closes. The ship gently lifts off as gangly landing legs retract. There's a beat, then a sudden whooshing sound as air rushes back into the space that previously held the craft, now suddenly vacuum. [NORAD][wen-norad] alarms go off briefly as an object is detected leaving the earth's atmosphere at a significant fraction of the speed of light.

In the years to come, many technological advances are made from what was left behind, a small tablet shaped object made of some kind of artificial stone/neutrino composite material. The alien message left on screen is eventually translated to read "Untitled Document 1 has not been saved, are you sure you wish to quit? (yes) (no) (cancel)"

Many years have passed, and we await the day the aliens return.

They have not.

With our new advancements, we build space-radar systems and can see the many species flying around the galaxy. It's not long before we realize they're intentionally giving earth a wide berth. Drone ships criss-cross the galaxy, but when they get within a lightyear of earth they detour around it. We finally get a subspace radio working, and start working to decode the noisy traffic of a thousand civilizations talking to each other. We broadcast a message of greetings and peace.

Less than a week later, the subspace net goes quiet. Our space radar reports the solar system is now surrounded by small vessels, suspected to be some kind of automated probe, and they're blocking all radio traffic in or out. Even the pulsars go quiet, all radio waves are gone. We focus on cracking the secret of FTL travel. The first prototype never makes it off the ground, as before the rocket can even ignite, it's crushed by a small meteor.

Forensic reconstruction suggests it was a sundial, carved from rock dug out of the far side of the moon.

(Foone, 2022)

</blockquote>

[wen-morristown-ufo-hoax]: https://en.wikipedia.org/wiki/Morristown_UFO_hoax
[wen-earths-orbit]: https://en.wikipedia.org/wiki/Earth's_orbit
[wen-leap-year]: https://en.wikipedia.org/wiki/Leap_year
[wen-february-patterns]: https://en.wikipedia.org/wiki/February#Patterns
[wen-birthdate-of-jesus]: https://en.wikipedia.org/wiki/Date_of_the_birth_of_Jesus
[wen-perspectives-on-jesus]: https://en.wikipedia.org/wiki/Religious_perspectives_on_Jesus
[wen-new-testament]: https://en.wikipedia.org/wiki/New_Testament
[wen-babylonian-numerals]: https://en.wikipedia.org/wiki/Babylonian_mathematics#Babylonian_numerals
[wen-am-pm]: https://en.wikipedia.org/wiki/12-hour_clock#Abbreviations
[wen-roman-empire]: https://en.wikipedia.org/wiki/Roman_Empire
[wen-si-prefixes]: https://en.wikipedia.org/wiki/Metric_prefix#List_of_SI_prefixes
[wen-roman-10-months-calendar]: https://en.wikipedia.org/wiki/Roman_calendar#Legendary_10-month_calendar
[wen-roman-republican-calendar]: https://en.wikipedia.org/wiki/Roman_calendar#Republican_calendar
[wen-ordinal-months]: https://en.wikipedia.org/wiki/Roman_calendar#Months
[wen-months-etymology]: https://en.wikipedia.org/wiki/Gregorian_calendar#Months
[wen-god-name-in-christianity]: https://en.wikipedia.org/wiki/God_in_Christianity#Name
[wen-god-name-in-islam]: https://en.wikipedia.org/wiki/God_in_Islam#Other_names
[wen-saturn-day]: https://en.wikipedia.org/wiki/Saturday
[wen-germanic-weekdays]: https://en.wikipedia.org/wiki/Names_of_the_days_of_the_week#Germanic_tradition
[wen-lunar-calendar-issues]: https://en.wikipedia.org/wiki/Month#Calendrical_consequences
[wen-babylonian-months]: https://en.wikipedia.org/wiki/Babylonian_calendar
[wen-latin-obscenity]: https://en.wikipedia.org/wiki/Latin_obscenity
[wen-interocitor]: https://en.wikipedia.org/wiki/This_Island_Earth_(novel)
[wen-norad]: https://en.wikipedia.org/wiki/NORAD

[^lunaciones]: N. del T.: Claramente el tema es tan complejo que nuestro colega terráqueo no es capaz de replicar adecuadamente. Aunque efectivamente el periodo de rotación de la Luna es de 27 días y ⅓ aproximadamente ([mes sideral](https://es.wikipedia.org/wiki/Mes#Mes_sideral)), los observadores _desde la Tierra_ tenemos en cuenta tácitamente la propia rotación de la Tierra en su desplazamiento alrededor del Sol, que es de unos 27º por cada rotación completa de la Luna. Esta observación _relativa_ resulta en el tiempo que pasa entre las distintas fases de la Luna vista desde la Tierra ([mes sinódico](https://es.wikipedia.org/wiki/Mes#Mes_sin%C3%B3dico) o lunación), que es de entre 29,27 y 29,83 días, y que sí forma alrededor de 12 meses por año solar.

