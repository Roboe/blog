---
title: "Cómo conectar mediante UART a una Raspberry Pi"
draftDate: 2024-04-14
date: 2024-06-30
categories:
  - guías
description: >
  Una pequeña guía técnica que describe cómo acceder a la consola serie de una Raspberry Pi mediante un adaptador UART a USB, proporcionando alimentación eléctrica sin un adaptador externo.
---

En esta publicación, como en la anterior, daré por hecho una familiaridad del lector con sistemas operativos Linux y con placas de desarrollo como la Raspberry Pi. No me detendré a explicar cada punto. Si no has entendido el título, probablemente este artículo no sea para ti, ¡salvo que tengas curiosidad!

En este caso, quería probar que un adaptador UART que compré, con intención de utilizarlo para instalar [postmarketOS](https://postmarketos.org/) en algun cacharro, funcionaba correctamente. Así que utilicé el primer dispositivo común de desarrollo que tenía a mano: una Raspberry Pi. Sin embargo, la cuestión no estaba tan bien documentada en internet como me hubiera gustado, así que aquí va mi contribución particular.


## Qué es la consola serie

Por lo general, existen distintas maneras de conectarse a dispositivos electrónicos desde otro dispositivo para realizar tareas, que normalmente son de administración.[^boot-logs]

En el caso de las placas de desarrollo Raspberry Pi o similares, lo más habitual es conectarse mediante el [protocolo Secure Shell](https://es.wikipedia.org/wiki/Secure_Shell) (SSH). Este protocolo de _software_ requiere establecer una conexión de red con el dispositivo, lo que significa que ambos dispositivos deben estar conectados con antelación a la misma red local, o bien que ambas redes sean accesibles por internet.

Sin embargo, existe otra manera de conectarse que utiliza la [interfaz Universal Asynchronous Receiver-Transmitter](https://es.wikipedia.org/wiki/Universal_Asynchronous_Receiver-Transmitter) (UART, por sus siglas en inglés), o en español, transmisor-receptor asíncrono universal. Al contrario que SSH, este «protocolo» no es de _software_, sino de _hardware_, así que requiere acceso físico al dispositivo objetivo. Para establecer la comunicación electrónica entre los dos dispositivos, se utiliza algún tipo de [cable serie](https://es.wikipedia.org/wiki/Cable_serie).

Esta tecnología está históricamente ligada al [puerto serie](https://es.wikipedia.org/wiki/Puerto_serie) físico de los ordenadores, así que también se suele referir a esta manera de acceso como transmisión serie o **consola serie**.


## Requisitos previos a conectar mediante UART

### Requisitos de _hardware_: obtener un adaptador UART

En el caso de este artículo, conectaremos a la consola serie de una Raspberry Pi 1 B+ desde un ordenador portátil con un sistema operativo Linux Mint Debian Edition. Además de esos dos dispositivos, necesitaremos:

- Un **adaptador USB a serie** con salida de 5&thinsp;V: los ordenadores portátiles no traen puertos serie ni chip UART, pero los puertos USB son prácticamente ubicuos. Necesitaremos un adaptador activo que nos permita establecer esa conexión. Generalmente vienen preparados para proporcionar 3,3&thinsp;V o 5&thinsp;V, para que sean compatibles con distintos dispositivos. Se encuentran por alrededor de 5&thinsp;€ en tiendas de electrónica; p. ej. [Conectrol](https://conectrol.com/producto/modulo-usb-a-uart-ttl-cp2102-5-pines/) (enlace no patrocinado). También se los denomina erróneamente adaptadores «TTL a USB».
- Cuatro **[cables puente](https://es.wikipedia.org/wiki/Cable_puente) hembra-hembra** (_jumper wires_): los cables que nos permitirán establecer, sin necesidad de soldar, las conexiones entre los cabezales de la placa con los cabezales del adaptador. Se encuentran en tiras de 10, 20 o 40 por 1-3&thinsp;€ en tiendas de electrónica; p. ej. [Conectrol](https://conectrol.com/producto/kit-cables-dupont-1p-para-protoboard-h-h-40-unds/) (enlace no patrocinado). También se los denomina en ocasiones «cables Dupont».

### Requisitos de _software_: preparar el sistema operativo de la Raspberry Pi

Las placas Raspberry Pi tienen unos cabezales de pines programables, los llamados GPIO, que permiten extender el _hardware_ de la placa de diversas maneras. Raspberry Pi OS, el sistema operativo Linux oficial para estas placas de desarrollo, ofrece la opción de exponer una consola serie UART en dos pines.[^uart-software]

Para ello, es necesario preconfigurar el sistema operativo mediante los archivos `config.txt` y `cmdline.txt` de la siguiente manera:

- Activar la opción `enable_uart=1` en el `config.txt` para configurar el cargador de arranque ([documentación](https://www.raspberrypi.com/documentation/computers/config_txt.html#enable_uart "Documentación oficial de Raspberry Pi para habilitar UART en el config.txt"))
- Añadir `console=serial0` o `console=serial0,115200` en el `cmdline.txt` para configurar el núcleo Linux ([documentación](https://www.raspberrypi.com/documentation/computers/configuration.html#kernel-command-line-cmdline-txt "Documentación oficial de Raspberry Pi OS para habilitar UART en el cmdline.txt"))


## Conectando a la consola serie

Cada modelo de placa Raspberry Pi sigue un diseño distinto para sus pines GPIO; es decir, cada uno tiene distinta funcionalidad. Debes consultar [la documentación](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html "Documentación de Raspberry Pi con información de los distintos pines GPIO") para confirmar qué pines conectar.

En el caso de mi Raspberry Pi 1 B+, los pines GPIO son los 14 y 15. Conectaré entonces mi adaptador UART primero al USB de mi ordenador, y después a los GPIO de la Raspberry Pi de la siguiente manera:

Adaptador UART &rarr; | Cable puente &rarr; | Raspberry Pi 1 B+
--- | --- | ---
GND Pin | negro | GND (Ground Pin)
RXD Pin | verde | TXD Pin (GPIO14)
TXD Pin | azul | RXD Pin (GPIO15)
+5V Pin (opcional) | rojo | 5V Pin 

El código de colores indicado arriba es para los cables puente. Es totalmente opcional, pero sigue el consenso habitual en electrónica. El pin de 5&thinsp;V es opcional: mediante este pin se alimenta eléctricamente la placa Raspberry Pi, y hacerlo así conlleva una serie de riesgos. Como se indica en la documentación oficial:

<figure>

> Los GPIO no tienen un regulador o un fusible para proteger del sobrevoltaje. Si el voltaje es incorrecto u ocurre un pico, la Raspberry Pi puede dañarse permanentemente.

</figure>

Alternativamente, se puede utilizar el adaptador de red habitual y no conectar este cuarto pin.

Una vez que alimentemos la placa, se encenderá y la comunicación empezará a fluir mediante UART. En ese momento, podemos conectar mediante un cliente en nuestro ordenador. Algunos conocidos son PuTTY o GNU Screen.

En mi caso, lanzaré desde una terminal el programa GNU Screen indicando el dispositivo virtual del adaptador y el baudaje de la conexión:

    $ screen /dev/ttyUSB0 115200

Cuando la conexión sea efectiva, tendremos una consola directa a nuestra placa de desarrollo. ¡Conseguido!


### Problema: imposible identificarse con el usuario por defecto

Si no personalizas la instalación de Raspberry Pi OS con el programa Raspberry Pi Imager, sino que grabas la imagen tal cual en la tarjeta SD, verás que no puedes identificarte con el usuario `pi` con contraseña por defecto `raspberry`, como fue habitual en el pasado.

La versión de Raspberry Pi OS lanzada en 2022-04-04 elimina la contraseña por defecto por motivos de seguridad. En su lugar, cuando se accede por primera vez, te permite establecer la contraseña, o espera que la establezcas al instalar el sistema con Raspberry Pi Imager. Sin embargo, por alguna razón, la consola que se abre por UART no te pregunta por una nueva contraseña.

Si te encuentras con este problema, es sencillo de solucionar con un poco de previsión. Antes de introducir la tarjeta en la Raspberry Pi, monta la partición en tu ordenador y elimina la `x` del fichero `$PUNTO_DE_MONTAJE/etc/passwd`. Cuando conectes, podrás iniciar sesión sin contraseña y configurarla después con el comando `passwd`.

~{{< author >}}


[^boot-logs]: La Raspberry Pi, además, también permite leer los mensajes del cargador de  arranque mediante UART para depurar errores de arranque, para tareas de desarrollo más avanzadas: https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#bootcode-bin-uart-enable

[^uart-software]: Recordemos que el protocolo UART es un protocolo de _hardware_, así que aquí estamos dando un doble salto: controlamos por _software_ el comportamiento del _hardware_ de la placa para que pueda conectarse a otro dispositivo, nuestro ordenador, mediante el protocolo UART.
