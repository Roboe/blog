---
title: "Reseña de la GodotCon 2023"
draftDate: 2023-11-12
date: 2023-11-13
categories:
  - reseñas
description: >
  La reseña de la conferencia anual sobre el software libre Godot: una comunidad acogedora, unos talleres de introducción magníficos y unas charlas deslumbrantes. Acompañado de mi buen amigo Carlos Padial, toda una experiencia memorable.
reposts:
  mastodon: https://mastodon.social/@RoboePi/111405777152907232
---

La semana pasada me encontraba volviendo de Múnich tras asistir junto con mi amigo y artista [Carlos Padial](https://surreal.asturnazari.com) a la [GodotCon 2023](https://conference.godotengine.org/2023/), una convención de la comunidad de Godot. [Godot](https://godotengine.org) es un motor de videojuegos 2D y 3D, y lo realmente apasionante es que es software libre. Los proyectos comunitarios que desbancan alternativas propietarias me resultan irresistibles.

![Cartel de la GodotCon 2023, 4 y 5 de noviembre de 2023 (en inglés), con el logo y mascota de Godot con un sombrero tirolés y la silueta de la ciudad de Múnich al fondo](godotcon23.png)

Pero un momento, ¿qué hacía yo allí, si no soy un ávido jugador ni desarrollo videojuegos? Pues porque pienso que [los videojuegos son una forma de arte]({{< relref mentes-corrientes-especial-arte >}}) y también una prometedora [herramienta de transformación social](https://arsgames.net).[^drm] Y porque asistir a eventos fuera de mi especialidad es muy enriquecedor, como ya experimenté al asistir a la [WordCamp 2018]({{< relref wordcamp-madrid-2018>}}).

[^drm]: Aunque no esté demasiado de acuerdo con el omnipresente DRM en la industria del videojuego, como [ya traté en este blog]({{< relref desmitificando-el-drm >}}).

También les tengo reservado un aprecio particular a los videojuegos por varios motivos personales. Uno, el emocional, porque fue modificando videojuegos como aprendí los principios básicos de la programación, llevado por mi curiosidad. Y otro, el intelectual, porque históricamente han sido una fuente de creatividad para superar limitaciones técnicas,[^pitfall] y eso es algo que me fascina de la inteligencia humana.

[^pitfall]: Mi ejemplo preferido es cómo _Pitfall!_ (1982) construía tantas distintas pantallas, una verdadera hazaña en una limitadísima Atari 2600. Puedes leer sobre el proceso, revelado mediante ingeniería inversa, en el blog del programador Jack Evoniuk (en inglés): https://evoniuk.github.io/posts/pitfall.html

Durante los dos días, hubo tres salas repletas de charlas y talleres. Naturalmente hay mucho que contar, pero he decidido destacar solo un puñado.


## Primer día: talleres

El primer día asistí a dos talleres —[el de TileMaps](https://pretalx.c3voc.de/godotcon2023/talk/HRBZK8/ "Mastering the 4.x TileMaps") y [el de Popochiu](https://pretalx.c3voc.de/godotcon2023/talk/39/ "Make point n' click games the easy way with Popochiu")— y a dos charlas —el relato de [la experiencia de un estudio indie](https://media.ccc.de/v/godotcon2023-57850-running-an-indie-game-dev-studio-based-on-godot-the-good-the-bad-and-the-ugly "Running an indie game dev studio based on Godot: the good, the bad and the ugly") y el de [la combinación de Swift con Godot](https://media.ccc.de/v/godotcon2023-57866-swift-godot-fixing-the-multi-million-dollar-mistake "Swift Godot: Fixing the Multi-million dollar mistake").

![Cartel del taller "Make point n' click games the easy way with Popochiu" de Mateo Robayo Rodríguez](godotcon23-popochiu-workshop.png)

Me gustaron más los talleres que las charlas, con diferencia, y destaco sin duda el taller de Popochiu que dio [Mateo Robayo Rodríguez](https://mapedorr.itch.io/). [Popochiu](https://mapedorr.itch.io/popochiu) es un _addon_ para Godot que permite construír aventuras gráficas al estilo de las de LucasArts, Sierra y varios otros clásicos. Habiendo disfrutado los títulos de Monkey Island, Mortadelo y Filemón o Broken Sword… no me lo podía perder.

Me pareció una herramienta con mucho trabajo detrás, sencilla en todo su potencial, y que hace muy accesible el desarrollo de estos videojuegos a personas con un conocimiento de programación justo. Además, aunque había algo de sueño por ser después de comer, el taller fue muy agradecido y fácil de seguir. Hicimos una pequeña aventura gráfica siguiendo a Mateo, que atendió varias incidencias y dudas. Punto extra para él porque, cuando vio que se agotaba el tiempo, improvisó su plan original para poder cerrar la historia sin perdernos ninguna de las funcionalidades clave.

Me quedé con ganas de ver la charla sobre [el futuro del renderizado en Godot](https://media.ccc.de/v/godotcon2023-57791-the-future-of-rendering-in-godot "The Future of Rendering in Godot") por Clay John, el líder del equipo de renderizado de Godot y un tipo realmente encantador en lo personal. Pero me alegro de haber asistido a los talleres porque esos no se grabaron.


## Segundo día: entre las matemáticas y los pinceles

El segundo y último día fue una serie de charlas, a cada cual más interesante y sorprendente. Primero [la historia y motivación tras GDScript](https://media.ccc.de/v/godotcon2023-57821-gdscript-past-present-and-future "GDScript: Past, Present, and Future"), luego [las vacas esféricas de simulación de vuelo](https://media.ccc.de/v/godotcon2023-57819--simplified-flight-simulation-library "(Simplified) Flight Simulation Library") y [la deconstrucción de una mecánica enmarañada](https://media.ccc.de/v/godotcon2023-57813-bending-wires-deconstruction-of-robot-detour-s-main-mechanic "Bending wires: Deconstruction of Robot Detour's main mechanic"), antes del [vistazo a los _viewports_](https://media.ccc.de/v/godotcon2023-57825-viewports-an-overview "Viewports: An Overview"). **¡Y eso solo antes de comer!** Después nos fascinó [la historia volumétrica de Bubblicius](https://media.ccc.de/v/godotcon2023-57815-volumetric-video-in-godot-the-making-of-strange-light-an-extended-reality-experience "Volumetric Video in Godot: The Making of Strange Light, an Extended-Reality Experience "), ver cómo se está usando [Godot en vehículos de alta gama](https://media.ccc.de/v/godotcon2023-57827-building-automotive-hmi-with-godot-rendercore "Building Automotive HMI with Godot & RenderCore") y [renderizados artísticos](https://media.ccc.de/v/godotcon2023-57824-from-watercolors-to-mechs-stylized-rendering-and-asset-pipelines-in-godot "From watercolors to mechs: Stylized rendering and asset pipelines in Godot") antes del [coloquio de cierre](https://media.ccc.de/v/godotcon2023-57831-fireside-chat "Fireside Chat").

La verdad es que tengo dificultades en destacar solo una, así que destacaré dos, :P

![Cartel de la charla "Bending wires: deconstruction of a complex game mechanic to simple concepts" de Ivan Bushmin](godotcon23-bending-wires.png)

[Ivan Bushmin](https://nozomu.ru/) nos planteó en su charla cómo abordar la implementación de la mecánica básica del juego de puzles arcade en el que está trabajando, [Robot Detour](https://nozomu57.itch.io/robot-detour). En él, controlas un robot cuyo cometido es transportar baterías a otros robots. Lo interesante de esto es que tu robot lleva un cable hacia su fuente de alimentación, y ese cable se enrolla y enmaraña por todo el nivel con tu movimiento. Debes dirigirlo inteligentemente para que no se dañe.

Ivan nos contó su proceso de pensamiento para dividir el problema por partes, y fue incrementando la complejidad de los casos que cubría sin perder la cordura. Así, nos introdujo a un problema geométrico que se puede resolver con matemáticas relativamente sencillas. Como buen profesional del desarrollo de videojuegos, la claridad en los pasos era nada desdeñable, y para finalizar nos mostró unos problemas extra, más avanzados, que está integrando en el juego, como cajas movedizas y otros elementos interactivos que mueven el cable de tu personaje.

![Cartel de la charla "Viewports: An Overview" de Raffaele Picca](godotcon23-viewports-overview.png)

[Raffaele Picca](https://www.raffaelepicca.com/), por su parte, nos sumergió en el fascinante mundo de los _viewports_, llevándonos de la mano por las distintas técnicas que utiliza, como apasionado de ellos y artistazo que es. Los _viewports_ son encuadres donde pintar, y Raffaele nos enseñó que los usa para cuatro objetivos distintos: para controlar la resolución gráfica del juego sin reducir la de los menús o los textos; para controlar distintas cámaras; para efectos visuales como [mezclar distintos entornos](https://bippinbits.itch.io/ravenhaul) en una misma visualización; o [para pintar](https://store.steampowered.com/app/2616650/Engraving/), literalmente, como con un pincel. Todo eso, con un análisis delicado de los pros y contras de cada uso.

Toda una explosión de posibilidades visuales del autor de 14 de los 15 _viewports_ que se usan en [Dome Keeper](https://store.steampowered.com/app/1637320/Dome_Keeper/).

Aún y con todo, no tuve tiempo de ver [lo máximo que se puede explotar gráficamente Godot](https://media.ccc.de/v/godotcon2023-57823-looking-good-achieving-the-best-graphical-quality-in-godot "Looking good!: Achieving the best graphical quality in Godot").


## Créditos finales

Es necesario y justo mencionar a todo el equipo organizador, que eran todos voluntarios de la comunidad de Godot, y el excelente trabajo que hicieron los moderadores presentando las charlas y coordinando las intervenciones. Y también al equipo de [grabación](https://media.ccc.de/c/godotcon2023) y emisión en directo del Chaos Computer Club. ¡Hurra!

Aunque los contenidos de la conferencia fueron excelentes, en realidad no fueron **ni la mitad de lo reseñable**. En esos pocos días en Múnich conocimos a un montón de gente, de todas partes de Europa y algunas sorprendentes del mundo: desde las cercanas Portugal y Cataluña, pasando por Italia, Malta, Suiza, Austria, Alemania, Dinamarca, Gran Bretaña, Islandia, Canadá, Estados Unidos y hasta Argentina. ¡Y eso solo de los que preguntamos!

También fueron memorables los ratos que Carlos y yo pasamos demostrando el juego de realidad virtual sobre rescatar cerditos en el que ha estado trabajando: [Piggy Rescue](https://asturnazari.itch.io/piggy-rescue) 🐷⛑️ (¡el código es abierto! [Échale un vistazo](https://github.com/surreal6/piggyRescue "Código del juego Piggy Rescue en GitHub")). Esto lo pudimos hacer en la zona habilitada en el espacio durante los dos días para enseñar tus juegos o prototipos a los demás.

Desde el primer momento tuve la sensación de que **la comunidad de Godot es especialmente amigable e inclusiva**, con personas de todos los bagajes, orígenes e identidades. Desde luego, no todas las comunidades _open source_ pueden presumir de ser así, algunas ni por asomo. ¡Que siga así de genial!

Un saludo especial desde aquí a Erik, Valentin, Federico, Kasper, Darío, Clay, Julian, Roger, Marc, Néstor, Mariana y Jeffrey, con los que compartimos paseos, charlas, comidas, bebidas y risas. ¡Esperemos encontrarnos de nuevo!

~{{< author >}}
