---
title: "Soy la lista de la compra de alguien en WhatsApp"
draftDate: 2023-08-27
date: 2023-08-31
categories:
  - reseñas
description: >
  Evalúo los tres años que cumplo sin WhatsApp y comparto, animado por un amigo, unas reflexiones propias que surgieron en una conversación de sobremesa, sobre la sociedad de la hiperconexión y la inteligencia artificial conversacional.
reposts:
  mastodon: https://mastodon.social/@RoboePi/110985429049965941
---

El otro día, en una conversación con mis amigos A., E. y R., después de hacer una reflexión sobre lo que llaman inteligencia artificial, E. me animó a que publicase una entrada.

Me hizo darme cuenta de que tengo el blog un poco desangelado. Así que sirva esta entrada para agradecerle desde aquí el haberme animado a volver a plasmar alguna anécdota y algunas reflexiones menores.


La anécdota y el chascarrillo
---

En esa conversación les conté que hacía un par de días, al reunirme con otra persona, me afeó que no le hubiera _leído_, y me explicó que me había escrito por chat para que le llevase algo que se había olvidado. Concretamente, me escribió por WhatsApp.

Como algunos sabréis, [en 2020 salí de ese servicio]({{< relref último-whatsapp >}}) a medio camino entre aplicación de mensajería, medio social y canal _de facto_ para comunicaciones profesionales y oficiales. Ya entonces denuncié que [mi cuenta había quedado en un estado zombi]({{< relref dos-semanas-sin-whatsapp >}}) en el que WhatsApp (Facebook/Meta) permite que mis contactos me sigan mandando mensajes, pero convenientemente no les dice que nunca llegarán. Y efectivamente, esta embarazosa anécdota tres años después de mi salida confirma que WhatsApp sigue secuestrando mensajes a las personas que se quieren comunicar conmigo con este patrón de diseño engañoso. Por si no fuera suficientemente desincentivador para este fin, es hasta posible que les lleve al equívoco de pensar que yo les he bloqueado.

Como buenos conversadores, seguimos el hilo por ahí, y entonces A. me descubrió un comportamiento de algunas personas que yo no conocía: resulta que hay quien utiliza las conversaciones en WhatsApp de las personas que les bloquean como bloc de notas personal. Conocía la práctica de aprovechar un grupo vaciado para eso, pero ¡no la conversación de alguien! Así que tomándomelo con humor, supongo que **debo ser la lista de la compra de alguien en WhatsApp**. Si me estás leyendo… ¡no te olvides de la leche!


Tres años sin WhatsApp
---

(Puedes saltar esta sección si vienes solo por la reflexión final)

Si todo va bien y publico esta entrada el día 31, entonces hará exactamente tres años desde que salí de WhatsApp. Me parece un buen momento para hacer un pequeño y honesto balance:[^balance-honesto]
- Mi agenda de Signal ha crecido, pero se mantiene principalmente para personas cercanas. Rara vez contesto al momento, porque tengo todas las notificaciones silenciadas[^signal-ermitaño] y no aparecen en la pantalla de bloqueo.
- Lamentablemente, no he podido deshacerme de Telegram aún por un par de grupos alrededor de actividades. Tengo configurada la cuenta para evitar nuevas conversaciones.[^telegram-capado]
- Mi uso del _smartphone_ ha decrecido.[^dónde-está-mi-móvil]
- Llamo por teléfono menos de lo que me gustaría. En este tiempo he compartido conversaciones telefónicas muy agradables.
- Cuando alguien quiere contactar conmigo, lo consigue. Sin embargo, no les resulta tan fácil hacerme llegar vídeos o contenido multimedia. Generalmenre no es un problema para mí.
- He mejorado mi atención y concentración. Leo textos largos con mayor facilidad y comprensión, y escucho a los demás con mayor interés y profundidad. Me he abierto a presenciar formas de arte que me atraían pero de alguna manera quedaban relegadas.
- He intensificado varias relaciones personales quedando en persona más. Especialmente con mi amigo R., con el que llevo dos años saliendo una vez por semana a pasear ociosamente. Le llamo mi amigo [peripatético](https://es.wikipedia.org/wiki/Escuela_peripat%C3%A9tica). Se lo recomiendo a todo el mundo.
- En nuevos grupos sociales, puedo quedar un tanto apartado al principio. No siempre entienden las razones de mi elección antes de conocerme mejor y ver cómo concibo la tecnología (no tienen porqué). En cualquier caso, casi siempre hay alguien que se preocupa o a quien puedo preguntar novedades, pero requiere una seguridad en ti mismo que puedes no tener.
- Siento que soy más tolerante y comprensivo con las ideas, maneras de vivir y elecciones personales de los demás, y ha sido una gran lección inesperada para mí.
- He recibido palabras bonitas, gestos de cariño y valoraciones muy sinceras de algunas personas cercanas, y me alegro profundamente de haberlas vivido en persona.

En general, considero el balance muy positivo. Aunque no todos los puntos son directamente achacables a mi decisión, sí considero que son derivados de la oportunidad de enriquecerme y progresar personalmente que me dio el tiempo de calidad que gané a partir de ella.

[^balance-honesto]: He querido incluír los puntos negativos y los controvertidos también en el balance. Esto no es Instagram y no es mi deseo mantener ninguna máscara de perfección y pureza falsas.

[^signal-ermitaño]: Signal tiene perfiles de notificaciones para adaptarlo a tus distintos ritmos diarios. Uso un perfil personalizado llamado «Ermitaño» que solo me notifica de un par de conversaciones seleccionadas. El resto las leo cuando entro voluntariamente.

[^telegram-capado]: Vaya por delante que el servicio propietario y centralizado de Telegram es igual de criticable que el de WhatsApp, incluso más en algunos aspectos como el del cifrado. Pero tiene un par de ventajas y un _hack_: puedo desactivar todas las notificaciones por completo; puedo no darle permiso a leer mi agenda telefónica y borrar los contactos guardados allí, y sigue funcionando sin interrupciones; y puedo indicarle que «solo mis contactos» (o sea, nadie) puedan hablarme o añadirme a grupos.

[^dónde-está-mi-móvil]: Lo uso tan poco que muchas veces me olvido de dónde lo he dejado dentro de casa, o de cambiar de conexión wifi a datos móviles cuando salgo a la calle.


Charlatanes disfrazados de avatares humanos
---

Continuando con aquella conversación, el diálogo giró en algún momento a compartir impresiones sobre el sistema conversacional vistoso, deslumbrante y de moda: ChatGPT. Yo no he llegado a utilizarlo: primero porque las tecnologías cerradas y asimétricas no me interesan; segundo, porque disfruto activamente de realizar las actividades humanísticas que este tipo de máquinas prometen automatizar; y tercero, porque definitivamente no «creo» en la inteligencia artificial. Al igual que la homeopatía, mi parecer es que no funcionan igual cuando no les presupones inteligencia, y en todos los casos hasta la fecha debes aportarles la tuya propia para que el resultado sea satisfactorio.

Algunos expertos se refieren a estos sistemas conversacionales como «loros estocásticos», en el sentido de que son capaces de imitar las complejas formas que nuestro lenguaje humano adopta, pero cualquier pretensión de sentido más allá es una mera conjetura. Tendemos a aportarle humanidad (en este caso, inteligencia humana) por el hecho de que algunos de sus rasgos no los hemos visto más que en humanos[^eldiario-elena] y la sincronicidad de la respuesta nos nubla el raciocinio (a todos; a mí el primero).

[^eldiario-elena]: La lingüista computacional Elena Álvarez Mellado analiza por qué nos parecen verosímiles estos sistemas conversacionales en este artículo: https://www.eldiario.es/tecnologia/pulpos-loros-sistemas-conversacionales_1_10421262.html


Nos acercamos a las máquinas
---

La reflexión germen de esta publicación viene de la mano de todo lo que he ido comentando en ella, y es una cuestión, en el fondo, sencilla. Llevamos más de una década inmersos e incrementando nuestras interacciones sociales a través de la tecnología. Probablemente «hables» más tiempo a través de chat que en persona, sepas más de la vida de tus amigos por _reels_ de Instagram, fotos, perfiles profesionales y microvídeos que por anécdotas verbales. Por esa razón, creo que hoy en día nuestras interacciones sociales son principalmente [parasociales](https://es.wikipedia.org/wiki/Interacci%C3%B3n_parasocial) y se alejan progresivamente de una verdadera conexión humana.

Además, como ya he dicho alguna vez y sigo sosteniendo, [las interfaces digitales nos homogeneizan]({{< relref dos-semanas-sin-whatsapp >}}), tratan de adaptarnos a un molde universal por pragmatismo. Con ello imposibilitan casi por completo la expresión honesta y mutifacética de nuestras singularidades y rasgos de la personalidad, especialmente aquellas que no son voluntarias, como los sentimientos.[^lingüística-mediática] Cualquiera que haya experimentado trabajar completamente en remoto durante alguna temporada ha podido experimentar desconexión emocional de sus compañeros. Es iluso esperar comunicación plena y honesta utilizando máscaras digitales que nos uniformizan. Los medios digitales tienen otras bondades, desde luego, pero no esas.

En este tiempo exponiéndonos cada vez más a través de interfaces digitales, hemos aprendido a aportar progresivamente más humanidad a las proyecciones virtuales de los demás. Además, nuestra homogeneidad son datos, y se pueden almacenar, cuantificar, etiquetar y analizar, como definitivamente hacen los sistemas de aprendizaje profundo. En ese contexto, ¿es realmente inteligente ChatGPT? Creo que **hace diez años no nos lo hubiera parecido para nada**. Cuanto más nos acerquemos y moldeemos nosotros mismos a la máquina en nuestras interacciones con otras personas, más «humanas» no parecerán las máquinas. Pero eso no tiene nada que ver con su naturaleza, sino con nuestra propia subestimación.

~{{< author >}}

[^lingüística-mediática]: Es indudable que la competencia comunicativa de los hablantes hace de esta limitación un tema lingüísticamente interesante. Tratar de «escribir como se habla», los sublenguajes de los emojis y los gifs o las interacciones de «me gustas» son genuínos testigos de la capacidad de adaptarnos al medio. Es una creatividad que surge de la limitación, como tantas otras, pero la limitación es evidente y criticable.
