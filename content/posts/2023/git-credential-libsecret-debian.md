---
title: "Cómo usar git por HTTPS con el llavero de Gnome en Debian"
draftDate: 2023-11-14
date: 2023-12-07
categories:
  - guías
description: >
  Una pequeña guía para integrar git con el llavero de Gnome y así almacenar tokens de acceso de manera segura para usar con repositorios mediante HTTPS.
reposts:
  mastodon: https://mastodon.social/@RoboePi/111397789738415735
---

Esta es una publicación un poco distinta: daré por hecho una familiaridad del lector con sistemas operativos Linux y con git.
El objetivo es documentar la solución de un problema técnico y no me detendré a explicar cada punto.
Si no has entendido el título, probablemente este artículo no sea para ti, ¡salvo que tengas curiosidad!


## El caso

Utilizo Debian ocasionalmente en un segundo ordenador donde no me gusta tener claves SSH.
Ahora las forjas de código, como GitLab o GitHub, impiden que uses tu contraseña en la terminal por seguridad.
En su lugar, te aconsejan utilizar tokens personales de acceso (PAT, por sus siglas en inglés).
Sin embargo, estos tokens no son memorizables como una contraseña, y también hay que protegerlos adecuadamente, así que no resultan tan cómodos.


## La hipótesis

Git delega la responsabilidad de almacenar secretos y claves de manera segura a otros sistemas, normalmente el almacenamiento del sistema operativo.
Para conectarse con ellos, git usa unos adaptadores que llama [_credential helpers_](https://git-scm.com/doc/credential-helpers).

Entonces, ¿debería haber un adaptador que permita a git utilizar el llavero de Gnome?


## El problema

En Linux no hay un solo almacenamiento seguro; el llavero de Gnome (Gnome Keyring) es uno de ellos, pero también lo es el monedero de KDE (KDE Wallet).
Para facilitar distintos almacenamientos en Linux, ambos implementan la [API de Secret Service](https://specifications.freedesktop.org/secret-service/latest/), un estándar de freedesktop.org.
[Libsecret](https://wiki.gnome.org/Projects/Libsecret) es una biblioteca de código para leer esos secretos usando la API de Secret Service.
El adaptador adecuado en este caso se llama `git-credential-libsecret`.

Sin embargo, en Debian hay una dificultad adicional.
En otras distribuciones de Linux, está disponible el binario precompilado de `git-credential-libsecret`.
Por ejemplo, en Arch Linux viene con el paquete de git ([Arch Wiki](https://wiki.archlinux.org/title/GNOME/Keyring#Git_integration)), y en Fedora Linux [tiene su propio paquete](https://packages.fedoraproject.org/pkgs/git/git-credential-libsecret/).
Desafortunadamente, ni Debian 11 «Bullseye» ni Debian 12 «Bookworm» proveen el binario precompilado.


## La solución

Afortunadamente, viendo los [contenidos del paquete de git en Debian](https://packages.debian.org/bookworm/amd64/git/filelist), encontré que incluye un archivo Makefile para compilar `git-credential-libsecret` tú mismo.

Primero, comprueba que efectivamente tengas la biblioteca Libsecret instalada:

    $ apt policy libsecret-1-0

Después, instala los archivos de desarrollo de Libsecret y compila `git-credential-libsecret` con Make:

    $ sudo apt install libsecret-1-dev
    $ cd /usr/share/doc/git/contrib/credential/libsecret
    $ sudo make

Finalmente, configura git para que use este _credential helper_:

    $ git config --global credential.helper $PWD/git-credential-libsecret

Opcionalmente, puedes desinstalar los archivos de desarrollo de Libsecret, porque ya no serán necesarios:

    $ sudo apt autoremove --purge libsecret-1-dev


## La conclusión

La siguiente vez que trates de conectar a un repositorio de git mediante HTTPS en la terminal, git te pedirá tu clave (el token personal de acceso).
En ese momento, lo guardará en el almacenamiento seguro, el llavero de Gnome en mi caso, mediante la biblioteca Libsecret.

A partir de entonces, siempre que el almacenamiento esté desbloqueado, ya lo utilizará automáticamente sin pedirte nada más. ¡Conseguido!

~{{< author >}}
