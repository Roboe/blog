---
title: "Mentes Corrientes #36: especial los límites del arte"
draftDate: 2023-11-13
date: 2021-11-16
categories:
  - reseñas
image: cartel.jpg
description: >
  Participo en el especial sobre los límites del arte del programa de radio Mentes Corrientes, en Ágora Sol Radio. Debato junto a una tatuadora, una artista circense y una programadora jugadora de videojuegos si sus disciplinas son consideradas formas de arte.
reposts:
  mastodon: https://mastodon.social/@RoboePi/107314788377158964
audio: https://archive.org/download/mentes_corrientes_036_temp3/mentes_corrientes_036_temp3.mp3?_=1
podcast:
  archiveOrg: https://archive.org/details/mentes_corrientes_036_temp3
  agoraSolRadio: https://www.agorasolradio.org/podcast/mentescorrientes/especial-los-limites-del-arte-ep-36-temp-iii/
---

Y... volví a [Mentes Corrientes](https://www.agorasolradio.org/podcast/mentescorrientes/ "Mentes Corrientes en Ágora Sol Radio"). Desde el programa de Ágora Sol Radio, Ester Legaz ([Twitter](https://twitter.com/EsterLegaz "Perfil de Twitter de Ester Legaz, presentadora de Mentes Corrientes")), Ana ([Instagram](https://instagram.com/narroana "Perfil de Instagram de Ana, presentadora de Mentes Corrientes")) y Loreto, sus presentadoras, han vuelto a contar conmigo para un programa más. Esta vez algo que me hacía especial ilusión, participar en una de sus interesantes mesas redondas, en una conversación cultural junto a seis artistazas: las mencionadas presentadoras, Teresa Cuttoo ([web](https://teresacuttoo.com/bio-teresa-cuttoo/), [Instagram](https://www.instagram.com/teresa_cuttoo/)), Noemí Burgos ([Instagram](https://www.instagram.com/noemiburgru/)) e Inés Pedraza Pino.


## El programa y el _podcast_

En este caso, el tema giraba alrededor de los límites del arte. Plantearon si se puede definir qué es arte, y tenían la leña preparada para debatir si los videojuegos, los tatuajes o el circo son arte. Durante una hora, nos sumergimos en estas preguntas, y alguna más profunda, como que si nuestra percepción de lo artístico cambia durante nuestra vida o cómo tratan estas disciplinas a la mujer y el feminismo.

<figure>

## Mentes&nbsp;Corrientes #36: especial los límites del arte

<br>

![Cartel del especial de los límites del arte con Teresa Cuttoo, Noemí Burgos, Roboe e Inés Pedraza Pino. Lunes 15 de noviembre a las 18:30h en Mentes Corrientes.](cartel.jpg)

<audio preload="metadata" controls style="width: 100%;">
  <source type="audio/mpeg" src="{{< param audio >}}" />
</audio>

No te pierdas las [notas al programa en la web de Ágora Sol Radio]({{< param "podcast.agoraSolRadio" >}}).

</figure>

¡Gracias por escuchar!

~{{< author >}}
