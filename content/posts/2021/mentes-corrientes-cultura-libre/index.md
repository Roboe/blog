---
title: "Mentes Corrientes #24: especial cultura libre"
draftDate: 2021-07-05
date: 2021-08-12
categories:
  - reseñas
image: cartel-1.jpg
description: >
  Participo con mi colega Luis en el especial sobre cultura libre del programa de radio Mentes Corrientes, en Ágora Sol Radio. Una introducción ligera y desenfadada a este abanico de temas, en el que se encuentra el DRM.
reposts:
  mastodon: https://mastodon.social/@RoboePi/106538885266054572
audio: https://archive.org/download/mentes_corrientes_024_temp2/mentes_corrientes_024_temp2.mp3?_=1
podcast:
  archiveOrg: https://archive.org/details/mentes_corrientes_024_temp2
  agoraSolRadio: https://www.agorasolradio.org/podcast/mentescorrientes/especial-cultura-libre-ep-24-temporada-ii/
  radioAlmaina: https://radioalmaina.org/2021/07/06/especial-cultura-libre-ep-24-temporada-ii/
---

El mes pasado, mi colega Luis ([Mastodon](https://mastodon.madrid/@lu1sdev "Perfil de Mastodon de Luis, colega y participante en este episodio")) y yo tuvimos la oportunidad de participar en un episodio especial de [Mentes Corrientes](https://www.agorasolradio.org/podcast/mentescorrientes/ "Mentes Corrientes en Ágora Sol Radio"), el programa de Ágora Sol Radio (en estos tiempos los llaman _podcasts_). Este programa, dirigido por Ester Legaz ([Twitter](https://twitter.com/EsterLegaz "Perfil de Twitter de Ester Legaz, presentadora de Mentes Corrientes")) y Ana ([Instagram](https://instagram.com/narroana "Perfil de Instagram de Ana, presentadora de Mentes Corrientes")), sirve de escenario para artistas emergentes y como altavoz para iniciativas culturales, y promueve conversaciones y debates sobre cultura en sus especiales y mesas redondas.

![Cartel del especial de cultura libre con Roboe y Luis de Dios, desarrolladores informáticos tecnoagnósticos. Lunes 5 de julio a las 18:30h en Mentes Corrientes. (Como fondo, la proyección de pantallas con código informático sobre la figura de una mujer.](cartel-1.jpg)

Durante toda esta segunda temporada del programa, la idea de hablar sobre digitalización e internet en este contexto cultural ha protagonizado varias conversaciones que he mantenido con Ester. Finalmente, se decidieron por este formato de especial para el gran abanico de temas de la cultura libre, y nos pusimos manos a la obra.

Para ser totalmente coherentes, nos preguntaron por una herramienta de colaboración de software libre para escribir el guión distinta de su habitual _Gobble_ Docs, así que recurrimos a [Etherpad](https://es.wikipedia.org/wiki/Etherpad) mediante [Framapad](https://framapad.org/es/), operado por la magnífica organización francesa sin ánimo de lucro [Framasoft](https://framasoft.org/es/), que ya nos dio buen servicio [cuando participé en Nación Lumpen]({{< relref nl18-tinfoil >}} "Reseña de mi participación en el podcast Nación Lumpen en 2018").


## El programa y el _podcast_

Introdujimos los temas con una perspectiva instructiva, sin dogmas y con ejemplos cercanos para todos. Con una duración de una hora, creemos haber llegado a un justo equilibrio entre conversación y contenido pero, sobre todo, **sin abrumar**. Ese era nuestro objetivo personal, pues tanto Luis como yo nos hemos encontrado múltiples veces con que, a la hora de explicar realidades complejas con facetas abstractas como la jurídica y la tecnológica, es un error frecuente querer transmitir demasiado en una sola dosis.

Además, lo pasamos muy bien, a pesar de los nervios que provoca el estar en frenético directo en un estudio de radio. Seguro que porque estas dos periodistas y presentadoras transmiten gran seguridad a sus invitados con su diligencia.

Como pequeño extracto del episodio, un resumen de preguntas que respondimos:

- De dónde vienen los derechos de autor.
- Cómo ha cambiado internet la creatividad.
- Qué son cultura libre y el software libre.
- Qué son las licencias Creative Commons y el DRM.

<figure>

## Mentes&nbsp;Corrientes #24: especial cultura libre

<br>

![Cartel del especial de cultura libre de Mentes Corrientes, en Ágora Sol Radio, con una cita del programa: «Seamos valientes. Seamos conscientes de la época en la que vivimos y de otras posibilidades que existen. (Como fondo, una mano señalando al frente y un grafo de tipo árbol sobrepuesto que parte del dedo índice)»](cartel-2.jpg)

<audio preload="metadata" controls style="width: 100%;">
  <source type="audio/mpeg" src="{{< param audio >}}" />
</audio>

No te pierdas las [notas al programa en la web de Ágora Sol Radio]({{< param "podcast.agoraSolRadio" >}}).

</figure>


Tras su reemisión en las ondas de la FM de Granada por [Radio Almaina]({{< param "podcast.radioAlmaina" >}}), el episodio del _podcast_ en la web de Ágora Sol Radio arrancó en el tercer puesto de los diez episodios de las dos últimas semanas más escuchados, y de ahí escaló hasta el primer puesto en los siguientes días. Allí se mantuvo hasta el día 22, en el que el algoritmo dejó de tenerlo en cuenta por salirse ya del periodo contemplado.

¡Gracias por escuchar!


~{{< author >}}

[enlace]: https://blog.virgulilla.com
